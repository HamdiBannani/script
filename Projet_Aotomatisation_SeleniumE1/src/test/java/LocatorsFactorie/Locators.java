/* Groupe: 1203
 * Cours: AUTOMATISATION DES TESTS II
 * Projet: Ninja
 * Etudiants (Nom & Prénom):
 * 			// Leila Fodil
 * 			// Nadia Jlidi
 * 			// Abir Doussouki
 * 			// Faiza Mehenni
 * 			// Yacine Bazizi
 * 			// Hamdi Bannani
 * 			// Ouassim Bellout
 * */
package LocatorsFactorie;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Locators {
	WebDriver driver;

// Constructeurs :
	public Locators(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

// Localisateurs Register page :
	@FindBy(xpath = "//span[contains(text(),'My Account')]")
	WebElement link_headerMyAccount;
	public WebElement getHeaderMyAccount() {
		return link_headerMyAccount;
	}

	@FindBy(xpath = "//a[contains(text(),'Register')]")
	WebElement link_registerMyAccount;
	public WebElement getRegisterMyAccount() {
		return link_registerMyAccount;
	}

	@FindBy(xpath = "//a[contains(.,'Login')]")
	WebElement link_loginMyAccount;
	public WebElement getLoginMyAccount() {
		return link_loginMyAccount;
	}

	@FindBy(xpath = "//input[@id='input-firstname']")
	WebElement link_firstNameRegister;
	public WebElement getFirstNameRegister() {
		return link_firstNameRegister;
	}

	@FindBy(xpath = "//input[@id='input-lastname']")
	WebElement link_lastNameRegister;
	public WebElement getLastNameRegister() {
		return link_lastNameRegister;
	}

	@FindBy(xpath = "//input[@id='input-email']")
	WebElement link_emailRegister;
	public WebElement getEmailRegister() {
		return link_emailRegister;
	}

	@FindBy(xpath = "//input[@id='input-telephone']")
	WebElement link_telephoneRegister;
	public WebElement getTelephoneRegister() {
		return link_telephoneRegister;
	}

	@FindBy(xpath = "//input[@id='input-password']")
	WebElement link_passwordRegister;
	public WebElement getPasswordRegister() {
		return link_passwordRegister;
	}

	@FindBy(xpath = "//input[@id='input-confirm']")
	WebElement link_confirmPasswordRegister;
	public WebElement getConfirmPasswordRegister() {
		return link_confirmPasswordRegister;
	}

	@FindBy(xpath = "//input[contains(@type,'checkbox')]")
	WebElement link_checkboxRegister;
	public WebElement getCheckboxRegister() {
		return link_checkboxRegister;
	}

	@FindBy(xpath = "//input[contains(@type,'submit')]")
	WebElement link_submitRegister;
	public WebElement getSubmitRegister() {
		return link_submitRegister;
	}

	@FindBy(xpath = "//a[contains(@class,'btn btn-primary')]")
	WebElement boutton_continueRegister;
	public WebElement getContinueRegister() {
		return boutton_continueRegister;
	}

	@FindBy(xpath = "//div[@id='top-links']/ul[@class='list-inline']//ul[@class='dropdown-menu dropdown-menu-right']//a[@href='http://tutorialsninja.com/demo/index.php?route=account/logout']")
	WebElement link_logoutRegister;
	public WebElement getLogoutRegister() {
		return link_logoutRegister;
	}

	@FindBy(xpath = "//a[contains(@class,'btn btn-primary')]")
	WebElement boutton_continueLogoutRegister;
	public WebElement getContinueLogoutRegister() {
		return boutton_continueLogoutRegister;
	}

	@FindBy(xpath = "//a[contains(text(),'Edit Account')]")
	WebElement link_editAccount;
	public WebElement getEditAccount() {
		return link_editAccount;
	}

	@FindBy(xpath = "//a[contains(.,'Your Store')]")
	WebElement link_headerMainPage;
	public WebElement getHeaderMainPage() {
		return link_headerMainPage;
	}
	
	@FindBy(xpath = "//input[@name='password']/following-sibling::a[1]")
	WebElement link_forgotPasswordLogin;
	public WebElement getForgotPasswordLogin() {
		return link_forgotPasswordLogin;
	}
	
	@FindBy(name = "search")
	WebElement link_searchHeaderPage;
	public WebElement getSearchHeaderPage() {
		return link_searchHeaderPage;
	}
	
	@FindBy(xpath = "//i[@class='fa fa-search']")
	WebElement button_searchHeaderPage;
	public WebElement getButton_searchHeaderPage() {
		return button_searchHeaderPage;
	}
	
	@FindBy(xpath = "(//span[text()='Add to Cart'])[1]")
	WebElement link_addToCart1;
	public WebElement getAddToCart1() {
		return link_addToCart1;
	}
	
	@FindBy(xpath = "//span[text()='Checkout']")
	WebElement link_checkout;
	public WebElement getCheckout() {
		return link_checkout;
	}
	
	@FindBy(xpath = "//a[contains(text(),'Shopping Cart')]")
	WebElement link_shoppingCart;
	public WebElement getShoppingCart() {
		return link_shoppingCart;
	}
	
	@FindBy(xpath = "//i[@class='fa fa-times-circle']")
	WebElement button_supprimAddCart;
	public WebElement getSupprimAddCart() {
		return button_supprimAddCart;
	}
	
}
